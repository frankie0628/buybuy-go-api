package main

import (
	"gopkg.in/gin-gonic/gin.v1"

	"buybuy-api/checksums"
	"buybuy-api/handlers"
	"buybuy-api/plugins"
)

func main() {
	router := gin.Default()

	router.Use(plugins.InjectMySQL())

	{
		router.GET("/", handler.Welcome)
		router.GET("/product", checksum.Product.SearchQuery)
	}

	// TODO: ADD authorzied check and remove user and user/:id
	pRouter := router.Group("/private")
	pRouter.Use(handler.User.InjectUser)
	{
		pRouter.POST("/user", checksum.User.NewUser, handler.User.Create)
		pRouter.GET("/user/:id",
			checksum.Common.CheckIdParam("id"), handler.User.GetById,
		)
		pRouter.POST("/user/:id/contact",
			checksum.User.NewContact, handler.User.CreateContact,
		)
		pRouter.GET("/user/:id/contact",
			checksum.User.SearchContactQuery, handler.User.SearchContact,
		)
		pRouter.POST("/user/:id/product",
			checksum.Product.NewProduct, handler.Product.Create,
		)
		pRouter.GET("/user/:id/product/:pid",
			checksum.Common.CheckIdParam("id", "pid"), handler.Product.GetById,
		)
	}

	router.Run()
}
