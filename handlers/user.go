package handler

import (
	"net/http"
	"strconv"

	"github.com/jinzhu/gorm"
	"gopkg.in/gin-gonic/gin.v1"

	"buybuy-api/models"
)

var User userMethod

func getById(db *gorm.DB, id uint) (model.User, bool) {
	var u model.User
	found := false
	user := u.GetById(db, id)

	if user.ID != 0 {
		found = true
	}

	return user, found
}

func (userMethod) InjectUser(ctx *gin.Context) {
	db := ctx.MustGet("mysql").(*gorm.DB)
	//TODO use firebase uid to verify id
	// then get user info from database
	uid := ctx.Request.Header.Get("user_id")
	id, _ := strconv.ParseUint(uid, 10, 64)
	user, found := getById(db, uint(id))

	if found {
		ctx.Set("user", user)
		ctx.Next()
	} else {
		ctx.Abort()
		ctx.JSON(http.StatusNotFound, gin.H{
			"message": "user(id:" + uid + ") not found",
		})
	}
}

func (userMethod) GetById(ctx *gin.Context) {
	user := ctx.MustGet("user").(model.User)

	ctx.JSON(http.StatusOK, user)
}

func (userMethod) Create(ctx *gin.Context) {
	db := ctx.MustGet("mysql").(*gorm.DB)
	user := ctx.MustGet("checksum").(model.User)

	if u := user.GetByUid(db, user.Uid); u.ID != 0 {
		ctx.JSON(http.StatusAccepted, gin.H{
			"user":    u,
			"message": "user already created",
		})
	} else {
		user.Create(db)

		ctx.JSON(http.StatusCreated, user)
	}
}

func (userMethod) CreateContact(ctx *gin.Context) {
	db := ctx.MustGet("mysql").(*gorm.DB)
	user := ctx.MustGet("user").(model.User)
	userContact := ctx.MustGet("checksum").(model.UserContact)

	userContact.UserID = user.ID
	userContact.Create(db)

	ctx.JSON(http.StatusCreated, userContact)
}

func (userMethod) SearchContact(ctx *gin.Context) {
	db := ctx.MustGet("mysql").(*gorm.DB)
	user := ctx.MustGet("user").(model.User)
	checksum := ctx.MustGet("checksum").(map[string]interface{})
	page := checksum["page"].(uint)
	startsWith := checksum["starts_with"].(string)

	result := user.SearchContact(db, page, startsWith)

	ctx.JSON(http.StatusOK, result)
}
