package handler

import (
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
)

func Welcome(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{"api": "Welcome to api"})
}
