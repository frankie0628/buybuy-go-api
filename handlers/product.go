package handler

import (
	"mime/multipart"
	"net/http"
	"sync"

	"buybuy-api/libs/s3"
	"buybuy-api/models"
	"github.com/jinzhu/gorm"
	"gopkg.in/gin-gonic/gin.v1"
)

var Product productMethod

func uploadImgs(images []*multipart.FileHeader) <-chan string {
	url := make(chan string)
	var wg sync.WaitGroup

	wg.Add(len(images))
	for _, image := range images {
		go func(i *multipart.FileHeader) {
			defer wg.Done()
			// TODO: HANDLE ERROR
			file, _ := image.Open()
			defer file.Close()

			// TODO: HANDLE ERROR
			resp, _ := awsS3.Upload(file, image.Filename)
			url <- resp
		}(image)
	}

	go func() {
		wg.Wait()
		close(url)
	}()

	return url
}

func createImgs(db *gorm.DB, urls <-chan string, uid uint, imageIds chan<- uint) {
	defer close(imageIds)
	for url := range urls {
		image := model.Image{Url: url, CreatorID: uid}
		image.Create(db)
		imageIds <- image.ID
	}
}

func createAddress(db *gorm.DB, addressId chan<- uint, data model.Product) {
	defer close(addressId)
	address := model.Address{
		Line:       data.GetAddressLine(),
		PostalCode: data.GetAddressPostalCode(),
		City:       data.GetAddressCity(),
		State:      data.GetAddressState(),
	}
	address.Create(db)
	addressId <- address.ID
}

func createProduct(db *gorm.DB, imgIds <-chan uint, data model.Product, aid <-chan uint, uid uint) model.Product {
	product := model.Product{
		Name:        data.Name,
		Description: data.Description,
		Price:       data.Price,
		CategoryID:  1,
		AddressID:   <-aid,
		CreatorID:   uid,
	}
	product.Create(db)

	for id := range imgIds {
		productImage := model.ProductImage{
			ProductID: product.ID,
			ImageID:   id,
		}
		productImage.Create(db)
	}

	return product.GetById(db, product.ID)
}

func (productMethod) Create(ctx *gin.Context) {
	user := ctx.MustGet("user").(model.User)
	db := ctx.MustGet("mysql").(*gorm.DB)
	data := ctx.MustGet("checksum").(map[string]interface{})

	imageData := data["images"].([]*multipart.FileHeader)
	productData := data["product"].(model.Product)

	imageIds := make(chan uint)
	addressId := make(chan uint)

	urls := uploadImgs(imageData)

	go createImgs(db, urls, user.ID, imageIds)
	go createAddress(db, addressId, productData)
	product := createProduct(db, imageIds, productData, addressId, user.ID)

	ctx.JSON(http.StatusCreated, product)
}

func (productMethod) GetById(ctx *gin.Context) {
	var p model.Product
	db := ctx.MustGet("mysql").(*gorm.DB)
	pid := ctx.MustGet("pid").(uint)

	product := p.GetById(db, pid)

	ctx.JSON(http.StatusOK, product)
}
