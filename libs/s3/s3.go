// TODO: REFACTOR ALL THE CONFIG INFO TO CONFIG FILE
package awsS3

import (
	"mime/multipart"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

var svc *s3.S3
var path = "https://s3-us-west-1.amazonaws.com/buybuy-img/"

func Upload(file multipart.File, filename string) (string, error) {
	_, err := svc.PutObject(&s3.PutObjectInput{
		Bucket: aws.String("buybuy-img"),
		Key:    aws.String(filename),
		Body:   file,
	})

	if err != nil {
		return "", err
	}

	return path + filename, nil
}

func init() {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	svc = s3.New(sess, aws.NewConfig().WithRegion("us-west-1"))
}
