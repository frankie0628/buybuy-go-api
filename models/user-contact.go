package model

import (
	"github.com/jinzhu/gorm"
	"time"
)

type UserContact struct {
	ID            uint       `sql:"AUTO_INCREMENT;primary_key" json:"id"`
	CreatedAt     time.Time  `sql:"not null;DEFAULT:current_timestamp" json:"created"`
	UpdatedAt     time.Time  `sql:"not null;DEFAULT:current_timestamp" json:"updated"`
	DeletedAt     *time.Time `json:"deleted"`
	UserID        uint       `sql:"not null;unique_index:uix_user_contact_id" json:"user_uid"`
	ContactID     uint       `sql:"not null;unique_index:uix_user_contact_id" json:"contact_id" form:"contact_id" binding:"required"`
	UnreadMessage uint       `sql:"not null;default:0;" json:"unread_message"`
}

func (u *UserContact) Create(db *gorm.DB) {
	db.Create(&u)
}
