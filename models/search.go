package model

type Search struct {
	Count uint          `json:"count"`
	Page  uint          `json:"page"`
	Rows  []interface{} `json:"rows"`
}
