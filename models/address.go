package model

import (
	"github.com/jinzhu/gorm"
	"time"
)

type Address struct {
	ID         uint       `json:"id" gorm:"AUTO_INCREMENT;primary_key"`
	CreatedAt  time.Time  `json:"created" gorm:"not null;default:current_timestamp"`
	UpdatedAt  time.Time  `json:"updated" gorm:"not null;default:current_timestamp"`
	DeletedAt  *time.Time `json:"deleted"`
	Line       string     `json:"line" form:"line" binding:"required" gorm:"not null;type:varchar(100)"`
	PostalCode string     `json:"postal_code" form:"postal_code" binding:"required" gorm:"not null;type:varchar(100)"`
	City       string     `json:"city" form:"city" binding:"required" gorm:"not null;type:varchar(100)"`
	State      string     `json:"state" form:"state" binding:"required" gorm:"not null;type:varchar(100)"`
}

func (a *Address) Create(db *gorm.DB) {
	db.Create(&a)
}
