package model

import (
	"github.com/jinzhu/gorm"
	"time"
)

type ProductImage struct {
	ID        uint       `json:"id" gorm:"AUTO_INCREMENT;primary_key"`
	CreatedAt time.Time  `json:"created" gorm:"not null;default:current_timestamp"`
	UpdatedAt time.Time  `json:"updated" gorm:"not null;default:current_timestamp"`
	DeletedAt *time.Time `json:"deleted"`
	ProductID uint       `json:"product_id" gorm:"not null;unique_index:uix_product_image_id"`
	ImageID   uint       `json:"image_id" gorm:"not null;unique_index:uix_product_image_id"`
}

func (pi *ProductImage) Create(db *gorm.DB) {
	db.Create(&pi)
}
