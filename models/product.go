package model

import (
	"github.com/jinzhu/gorm"
	"time"
)

type Product struct {
	ID          uint       `json:"id" gorm:"AUTO_INCREMENT;primary_key"`
	CreatedAt   time.Time  `json:"created" gorm:"not null;default:current_timestamp"`
	UpdatedAt   time.Time  `json:"updated" gorm:"not null;default:current_timestamp"`
	DeletedAt   *time.Time `json:"deleted"`
	Name        string     `json:"name" form:"name" binding:"required" gorm:"not null;type:varchar(200)"`
	Description string     `json:"description" form:"description" binding:"required" gorm:"not null;type:varchar(5012)"`
	Price       float32    `json:"price" form:"price" binding:"required" gorm:"not null;type:decimal(19, 2)"`
	CategoryID  uint       `json:"product_category_id" gorm:"not null;index:idx_cateogry_id"`
	AddressID   uint       `json:"address_id" gorm:"not null;index:idx_address_id"`
	CreatorID   uint       `json:"creator_id" gorm:"not null;index:idx_creator_id"`
	Address     Address    `json:"address" gorm:"foreignkey:AddressID"`
	Creator     User       `json:"creator" binding:"-" gorm:"foreignkey:CreatorID"`
	Category    Category   `json:"category" gorm:"foreignkey:CategoryID"`
	Images      []Image    `json:"product_images" gorm:"many2many:product_image"`
}

func (p *Product) GetAddressLine() string {
	return p.Address.Line
}

func (p *Product) GetAddressPostalCode() string {
	return p.Address.PostalCode
}

func (p *Product) GetAddressCity() string {
	return p.Address.City
}

func (p *Product) GetAddressState() string {
	return p.Address.State
}

func (p *Product) Create(db *gorm.DB) {
	db.Create(&p)
}

func (*Product) GetById(db *gorm.DB, id uint) Product {
	var product Product
	db.Preload("Address").Preload("Category").Preload("Creator").Preload("Images").Where("id = ?", id).First(&product)

	return product
}
