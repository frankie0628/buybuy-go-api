package model

import (
	"github.com/jinzhu/gorm"
	"time"
)

type User struct {
	ID            uint       `sql:"AUTO_INCREMENT;primary_key" json:"id"`
	CreatedAt     time.Time  `sql:"not null;default:current_timestamp" json:"created"`
	UpdatedAt     time.Time  `sql:"not null;default:current_timestamp" json:"updated"`
	DeletedAt     *time.Time `json:"-"`
	Uid           string     `sql:"not null;type:varchar(100);unique_index" json:"uid" form:"uid" binding:"required"`
	Email         string     `sql:"not null;type:varchar(100);unique_index" json:"email" form:"email" binding:"required,email"`
	EmailVerified uint8      `sql:"not null" json:"email_verified" form:"email_verified" binding:"gte=0,lte=1"`
	Name          string     `sql:"not null;type:varchar(100)" json:"name" form:"name" binding:"required"`
	ProfileImage  string     `sql:"type:varchar(250)" json:"profile_image" form:"profile_image" binding:"omitempty,url"`
}

func (u *User) Create(db *gorm.DB) {
	db.Create(&u)
}

func (*User) GetById(db *gorm.DB, id uint) User {
	var user User
	db.Where("id = ?", id).First(&user)

	return user
}

func (*User) GetByUid(db *gorm.DB, uid string) User {
	var user User
	db.Where("uid = ?", uid).First(&user)

	return user
}

func (u *User) SearchContact(db *gorm.DB, page uint, startsWith string) Search {
	var users []User
	var searchResult Search

	// REFACTOR THIS TO search.go
	db.Select(
		"SQL_CALC_FOUND_ROWS `user`.*",
	).Joins(
		"join `user_contact` on `user_contact`.`contact_id` = `user`.`id`",
	).Where(
		"`user_contact`.`user_id` = ? AND `name` LIKE ?", u.ID, startsWith+"%",
	).Limit(
		2,
	).Offset(
		(page - 1) * 2,
	).Find(&users)

	// ATTENTION: DONT DO TWO SEARCH AT THE SAME TIME!!!
	db.Raw("SELECT FOUND_ROWS() AS count;").Scan(&searchResult)

	searchResult.Page = page
	searchResult.Rows = make([]interface{}, 0)

	for _, user := range users {
		searchResult.Rows = append(searchResult.Rows, user)
	}

	return searchResult
}
