package model

import "time"

type Category struct {
	ID        uint       `json:"id" gorm:"AUTO_INCREMENT;primary_key"`
	CreatedAt time.Time  `json:"created" gorm:"not null;default:current_timestamp"`
	UpdatedAt time.Time  `json:"updated" gorm:"not null;default:current_timestamp"`
	DeletedAt *time.Time `json:"deleted"`
	Name      string     `json:"name" form:"category_name" binding:"required" gorm:"not null;type:varchar(100)"`
}
