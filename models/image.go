package model

import (
	"github.com/jinzhu/gorm"
	"time"
)

type Image struct {
	ID        uint       `json:"id" sql:"AUTO_INCREMENT;primary_key"`
	CreatedAt time.Time  `json:"created" sql:"not null;default:current_timestamp"`
	UpdatedAt time.Time  `json:"updated" sql:"not null;default:current_timestamp;on update current_timestamp"`
	DeletedAt *time.Time `json:"deleted"`
	CreatorID uint       `json:"creator_id" sql:"not null"`
	Url       string     `json:"url" binding:"required" sql:"not null;type:varchar(256)"`
}

func (i *Image) Create(db *gorm.DB) {
	db.Create(&i)
}
