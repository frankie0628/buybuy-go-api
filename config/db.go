package config

import "github.com/jinzhu/configor"

var DB = struct {
	MySQL struct {
		Username     string `default: "root"`
		Password     string `default: "root"`
		Protocal     string `default: "tcp"`
		Address      string `default: "127.0.0.1"`
		Port         string `default: "3306"`
		DatabaseName string `default: "test"`
	}
}{}

func init() {
	if err := configor.Load(&DB, "config/database.yml"); err != nil {
		panic(err)
	}
}
