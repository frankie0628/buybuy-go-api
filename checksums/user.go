package checksum

import (
	"net/http"

	"gopkg.in/gin-gonic/gin.v1"

	"buybuy-api/models"
)

type userCheckSum struct{}

var User userCheckSum

func (userCheckSum) NewUser(ctx *gin.Context) {
	user := model.User{}

	// Bind method from gin is having a problem setting content type
	// wait for next update from gin
	ctx.Header("Content-Type", "application/json; charset=utf-8")

	if err := ctx.Bind(&user); err != nil {
		errorMessage := createErrMsg(err)
		ctx.Abort()
		ctx.JSON(http.StatusBadRequest, errorMessage)
	} else {
		ctx.Set("checksum", user)
		ctx.Next()
	}
}

func (userCheckSum) NewContact(ctx *gin.Context) {
	userContact := model.UserContact{}

	if err := ctx.Bind(&userContact); err != nil {
		ctx.Abort()
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err})
	} else {
		ctx.Set("checksum", userContact)
		ctx.Next()
	}
}

func (userCheckSum) SearchContactQuery(ctx *gin.Context) {
	page, err := getPageQuery(ctx)

	if err != nil {
		ctx.Abort()
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "query page is not a uint number",
		})
	} else {
		startsWith := ctx.Query("starts_with")
		checksum := make(map[string]interface{})
		checksum["page"] = page
		checksum["starts_with"] = startsWith
		ctx.Set("checksum", checksum)
		ctx.Next()
	}
}
