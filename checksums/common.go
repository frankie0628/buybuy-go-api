package checksum

import (
	"net/http"
	"strconv"

	"gopkg.in/gin-gonic/gin.v1"
)

type commonCheckSum struct{}

var Common commonCheckSum

func (commonCheckSum) CheckIdParam(namespace ...string) func(*gin.Context) {
	return func(ctx *gin.Context) {
		for _, name := range namespace {
			id, err := strconv.ParseUint(ctx.Param(name), 10, 64)
			if err != nil {
				ctx.Abort()
				ctx.JSON(http.StatusBadRequest, gin.H{"message": "id must be uint"})
				break
			} else {
				ctx.Set(name, uint(id))
			}
		}

		ctx.Next()
	}
}
