package checksum

// import (
// 	"log"
// 	"mime/multipart"
// 	"net/http"

// 	// "github.com/aws/aws-sdk-go/aws"
// 	// "github.com/aws/aws-sdk-go/aws/credentials"
// 	// "github.com/aws/aws-sdk-go/aws/session"
// 	// "github.com/aws/aws-sdk-go/service/s3"
// 	"gopkg.in/gin-gonic/gin.v1"
// )

// var Image imageCheckSum

// func isImage(file multipart.File) (bool, error) {
// 	buffer := make([]byte, 512)
// 	_, err := file.Read(buffer)
// 	if err != nil {
// 		log.Println("READ ERROR ", err)
// 		return false, err
// 	}

// 	contentType := http.DetectContentType(buffer)

// 	for _, imageType := range imageTypes {
// 		if contentType == imageType {
// 			return true, nil
// 		}
// 	}

// 	return false, nil
// }

// func (imageCheckSum) NewImage(ctx *gin.Context) {
// 	err := ctx.Request.ParseMultipartForm(defaultMemory)
// 	if err != nil {
// 		log.Println(err)
// 	}
// 	form := ctx.Request.MultipartForm
// 	defer form.RemoveAll()
// 	files := form.File["images"]

// 	for _, file := range files {
// 		data, openErr := file.Open()
// 		defer data.Close()
// 		if openErr != nil {
// 			log.Println("OPEN ERROR %v", openErr)
// 			return
// 		}

// 		if pass, _ := isImage(data); !pass {
// 			ctx.Abort()
// 			ctx.JSON(http.StatusBadRequest, gin.H{"message": "Invalid Image Type"})
// 		}
// 	}

// 	ctx.Set("checksum", files)
// 	ctx.Next()

// sess := session.Must(session.NewSessionWithOptions(session.Options{
// 	SharedConfigState: session.SharedConfigEnable,
// }))

// svc := s3.New(sess, aws.NewConfig().WithRegion("us-west-1"))

// for _, file := range files {
// 	val, openErr := file.Open()
// 	if openErr != nil {
// 		log.Println("Open Error ", openErr)
// 	}
// 	defer val.Close()

// 	_, uploadErr := svc.PutObject(&s3.PutObjectInput{
// 		Bucket: aws.String("buybuy-img"),
// 		Key:    aws.String(file.Filename),
// 		Body:   val,
// 	})

// 	if uploadErr != nil {
// 		log.Println("Upload Error ", uploadErr)
// 	}

// 	log.Printf("Successfully uploaded %v to buybuy-img", file.Filename)
// }

// }
