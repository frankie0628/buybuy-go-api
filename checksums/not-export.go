package checksum

import (
	"strconv"

	"gopkg.in/gin-gonic/gin.v1"
	"gopkg.in/go-playground/validator.v8"
)

const (
	defaultMemory = 32 << 20
)

var (
	imageTypes = []string{"image/jpeg", "image/gif", "image/png", "image/bmp"}
)

type validationFormat struct {
	Tag   string `json:"tag"`
	Param string `json:"param"`
}

func createErrMsg(err error) map[string]validationFormat {
	msg := make(map[string]validationFormat)
	validationError, _ := err.(validator.ValidationErrors)

	for _, v := range validationError {
		msg[v.FieldNamespace] = validationFormat{v.Tag, v.Param}
	}

	return msg
}

func toUint(num string) (uint, error) {
	result, err := strconv.ParseUint(num, 10, 64)

	if err != nil {
		return 0, err
	} else {
		return uint(result), nil
	}
}

func getPageQuery(ctx *gin.Context) (uint, error) {
	page := ctx.Query("page")
	if page == "" {
		page = "1"
	}

	return toUint(page)
}

func getCidQuery(ctx *gin.Context) (uint, error) {
	cid := ctx.Query("cid")
	if cid == "" {
		cid = "0"
	}

	return toUint(cid)
}

func getPriceMinQuery(ctx *gin.Context) (uint, error) {
	priceMin := ctx.Query("priceMin")
	if priceMin == "" {
		priceMin = "0"
	}

	return toUint(priceMin)
}
