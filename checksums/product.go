package checksum

import (
	"log"
	"mime/multipart"
	"net/http"

	"gopkg.in/gin-gonic/gin.v1"

	"buybuy-api/models"
)

type productCheckSum struct{}

var Product productCheckSum

func isImage(file multipart.File) (bool, error) {
	buffer := make([]byte, 512)
	_, err := file.Read(buffer)
	if err != nil {
		log.Println("READ ERROR ", err)
		return false, err
	}

	contentType := http.DetectContentType(buffer)

	for _, imageType := range imageTypes {
		if contentType == imageType {
			return true, nil
		}
	}

	return false, nil
}

func validateImageFiles(ctx *gin.Context) ([]*multipart.FileHeader, bool) {
	err := ctx.Request.ParseMultipartForm(defaultMemory)
	if err != nil {
		log.Println(err)
	}
	form := ctx.Request.MultipartForm
	defer form.RemoveAll()
	files := form.File["images"]

	for _, file := range files {
		data, openErr := file.Open()
		defer data.Close()
		if openErr != nil {
			log.Println("OPEN ERROR %v", openErr)
			return nil, false
		}

		if pass, _ := isImage(data); !pass {
			return nil, false
		}
	}

	return files, true
}

func (productCheckSum) NewProduct(ctx *gin.Context) {
	product := model.Product{}
	data := make(map[string]interface{})

	ctx.Header("Content-Type", "application/json; charset=utf-8")

	if files, pass := validateImageFiles(ctx); !pass {
		ctx.Abort()
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "Invalid Image"})
		return
	} else if len(files) == 0 {
		ctx.Abort()
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "Image is required"})
		return
	} else {
		data["images"] = files
	}

	if err := ctx.Bind(&product); err != nil {
		errorMessage := createErrMsg(err)
		ctx.Abort()
		ctx.JSON(http.StatusBadRequest, errorMessage)
	} else {
		data["product"] = product
		ctx.Set("checksum", data)
		ctx.Next()
	}
}

func (productCheckSum) SearchQuery(ctx *gin.Context) {
	page, err := getPageQuery(ctx)

	if err != nil {
		ctx.Abort()
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid query param",
		})
	} else {
		checksum := make(map[string]interface{})
		checksum["page"] = page
		checksum["keyword"] = ctx.Query("keyword")
		// checksum["cid"] = c

		ctx.Set("checksum", checksum)
		ctx.JSON(200, "ok")
	}
}
