package plugins

import (
	"bytes"
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gopkg.in/gin-gonic/gin.v1"

	"buybuy-api/config"
	"buybuy-api/models"
)

func getDsn() string {
	var dsn bytes.Buffer

	dsn.WriteString(config.DB.MySQL.Username)
	dsn.WriteString(":")
	dsn.WriteString(config.DB.MySQL.Password)
	dsn.WriteString("@")
	dsn.WriteString(config.DB.MySQL.Protocal)
	dsn.WriteString("(")
	dsn.WriteString(config.DB.MySQL.Address)
	dsn.WriteString(":")
	dsn.WriteString(config.DB.MySQL.Port)
	dsn.WriteString(")/")
	dsn.WriteString(config.DB.MySQL.DatabaseName)
	dsn.WriteString("?charset=utf8&parseTime=True&loc=Local")

	return dsn.String()
}

func InjectMySQL() gin.HandlerFunc {
	db, err := gorm.Open("mysql", getDsn())

	if err != nil {
		fmt.Print(err)
		fmt.Print(getDsn())
		panic(err)
	}

	db.LogMode(true)
	db.SingularTable(true)
	db.AutoMigrate(
		&model.User{},
		&model.UserContact{},
		&model.Product{},
		&model.ProductImage{},
		&model.Image{},
		&model.Category{},
		&model.Address{},
	)

	return func(ctx *gin.Context) {
		ctx.Set("mysql", db)
		ctx.Next()
	}
}
